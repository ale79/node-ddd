'use strict';

//Aggregate root creates events, but this class is the only one allowed to publish them
//AR doesn't have any knowldedge about infrastructure (i.e. SQS/SNS/ActiveMQ), this entity fills the gap
//It is usually called from Repository after saving the AR 
class EventPublisher{
    constructor(sdk){
        this._publishedEvents = [];
        this._sdk = sdk;
    }
    
    publish(aggregateRoot){
        //1) Raise pending events
        aggregateRoot._events.forEach(event => {
            // sdk.sns.send(event)
            //attach correlationId
            this._publishedEvents.push(event);
        });

        //2) Clear events
        aggregateRoot.clearEvents();
    }
}

module.exports = EventPublisher;