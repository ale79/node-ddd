'use strict';

var Order = require('../domain/Order');
var LineItem = require('../domain/LineItem');

//Aggregate Roots need to be saved/loaded atomically, i.e. when we save/read Order, we also save/read all the line items
//We shouldn't need to create a repository for line items
class OrderRepository{
    constructor(eventPublisher, ct_sdk){
        this._publisher = eventPublisher;
        this._ct_sdk = ct_sdk;
    }

    save(order){
        //1) Save changes -- ex. post order to CT
        //this._ct_sdk.updateOrder...

        //2) Publish pending events
        this._publisher.publish(order);
    }

    get(id){
        var order = new Order(); //parameterless ctor call to avoid OrderCreated event
        order._id = id;

        //this._ct_skd.getOrderById(id)...

        //Some mock data for the sake of the example
        var shippedLineItem = new LineItem(1);
        shippedLineItem.markAsShipped();
        order._lineItems = [shippedLineItem, new LineItem(2)]; 

        return order;  
        //There is no point to use the event publisher here, since we have just retrieved the AR
    }
}

module.exports = OrderRepository;