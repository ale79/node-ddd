const express = require('express');
const EventHandlerDemo = require('./EventHandlerDemo');

const app = express();


app.get('/', (req,res) =>{
    var orderItemShippedMessage = JSON.stringify({correlationId:'xyz', payload:{ orderId: 1, lineItemId:2}});
    var result = new EventHandlerDemo().go(orderItemShippedMessage);

    res.send('done!');
  });

app.listen(3000);


