'use strict';

const EventHandlerBuilder = require('./pipeline/EventHandlerBuilder');
const OrderItemShippedHandler = require('./pipeline/OrderItemShippedHandler');
const MessageLoggerMiddleware = require('./pipeline/middleware/messageLoggerMiddleware');
const CorrelationIdMiddleware = require('./pipeline/middleware/correlationIdMiddleware');


module.exports = class EventHandlerDemo{
    constructor(){}

    go(orderItemShippedMessage){       
    
        //This can be at the startup of the lambda
        var handler = new EventHandlerBuilder(OrderItemShippedHandler)
                        .withMiddleware(MessageLoggerMiddleware)
                        .withMiddleware(CorrelationIdMiddleware)
                        .build();

        return handler.handle(orderItemShippedMessage);
    }
}
