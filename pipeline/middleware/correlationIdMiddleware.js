'use strict';

module.exports = class CorrelationIdMiddleware{
    constructor(innerHandler){
        this._handler = innerHandler;
    }

    handle(message){
        var event = JSON.parse(message);
       
        var correlationId = event.correlationId;
        console.log('CorrelationId found:'+correlationId);
        //assign the correlation Id to eventPublisher class or somewhere where it can access to

        var result = this._handler.handle(message);
       
        return result;
    }
}