'use strict';

module.exports = class messageLoggerMiddleware{
    constructor(innerHandler){
        this._handler = innerHandler;
    }

    handle(message){
        console.log(message);
        var result = this._handler.handle(message);
        console.log(result);

        return result;
    }
}


