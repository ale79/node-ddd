'use strict';

const OrderRepository = require('../infrastructure/orderRepository');
const EventPublisher = require('../infrastructure/eventPublisher');

module.exports = class OrderItemShippedHandler {
    constructor() { }
    handle(message) {
        var event = JSON.parse(message).payload;
        var repo = new OrderRepository(new EventPublisher());
        var order = repo.get(event.orderId);
        order.shipItem(event.lineItemId);
        repo.save(order);
        return repo._publisher._publishedEvents;
    }
}