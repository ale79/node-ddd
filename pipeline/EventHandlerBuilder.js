'use strict';

module.exports = class EventHandlerBuilder {
    constructor(handler) {
        this._handler = new handler();
        this._middlewares = [];
    }
    withMiddleware(middleware) {
        this._handler = new middleware(this._handler);
        return this;
    }
    build() {
        return this._handler;
    }
}