'use strict';

//LineItem is an entity, not an aggregate root. It depends on Order aggregate to exist
//This class shouldn't create events, nor saved without its aggregate
module.exports = class LineItem{
    constructor(lineItemId){
        if(!arguments.length) { 
            //parameterless constructor should only be used by repository when loading an entity
            //this is not strictly needed with non-aggregate classes, since they don't register events
            //but it is good to keep consistency within repository loading approach
            return;
        }

        this._lineItemid = lineItemId;
        this._shipped = false;
    }

    get lineItemId(){
        return this._lineItemid;
    }

    isShipped(){
        return this._shipped;
    }

    markAsShipped(){
        this._shipped = true;
    }
}