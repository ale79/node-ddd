'use strict';

const OrderItemConfirmedEvent = require('./events/orderItemConfirmedEvent');
const OrderConfirmedEvent = require('./events/orderConfirmedEvent');

//Aggregate Root
//This class needs to be saved atomically, together with its child dependencies
class Order {
    
    constructor(orderId, lineItems){
        this._events = [];  //Only aggregate root defines events, lineItem entity shouldn't care

        if(!arguments.length) { 
            //parameterless constructor should only be used by repository when loading the object
            //we don't want to raise event on this scenario
            return;
        }

        this._id = orderId;
        this._lineItems = lineItems;
        //...

        this._events.push(new OrderCreatedEvent(orderId, lineItems)); 
    }

    isSubmitted(){
        //Eg. this._state === 'submitted'
        return true; 
    }

    shipItem(lineItemId){
        //Order Invariant check
        //Invariant must be met on every method call
        //It is important to ensure that writing operations are wrapped in function/setter calls, 
        //so that we can run invariant checks and workout if we need to create ank event
        //An aggregate root cannot be saved if its invariant is not respected
        if (!this.isSubmitted()){
            throw InvalidOrderStateException(); 
        }   
        // invariant end


        var currentItem = this._lineItems.find(i => i.lineItemId == lineItemId);
        if (currentItem.isShipped()){
            //no action
            return;
        }

        currentItem.markAsShipped();

        //event is based on Order entity logic, not delivery logic
        //extracts relevant info
        this._events.push(new OrderItemConfirmedEvent({orderNumber:this._id, itemId: currentItem.id}));

        //Aggregate root looks into child entities and based on their state decides to 'raise' an event
        if (this._lineItems.every (li => li.isShipped())){
            this._events.push(new OrderConfirmedEvent({orderNumber:this._id}))
        }
    }

    addLineItem(lineItem){
        this._lineItems.push(lineItem);
        this._events.push(new LineItemAddedEvent({orderId:this._id, lineItem:lineItem}));
    }

    //Aggregate root related
    get events(){
        return this._events;
    }

    clearEvents(){
        this._events.length = 0;
    }

}

module.exports = Order;
